#!/bin/bash

TO_TEST="${1}"

if [ "${TO_TEST}" = "get" ]
then

curl localhost:4000/v1/rings

fi

if [ "${TO_TEST}" = "create" ]
then

curl -0 -X POST http://localhost:4000/v1/rings \
-H 'Content-Type: application/json' \
--data-binary @- <<EOF
{
  "name": "Konoha",
  "domain": "konoha.ring.io",
  "creator": "hokage",
  "admin": "kakashi",
  "password": "p4ssword",
  "tags": ["ninjas", "naruto"]
}
EOF
fi

if [ "${TO_TEST}" = "update" ]
then

echo "Testing update"
curl -X PUT "localhost:4000/v1/rings/6249878ff2766f4d5574dd9a/status?status=PROCESSING&xmppPort=1200&apiPort=4000"

fi
