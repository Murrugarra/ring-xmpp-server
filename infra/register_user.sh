HOST_NAME=$1
USERNAME=$2
PASSWORD=$3

docker exec -it "ring-${HOST_NAME}" bin/ejabberdctl register "${USERNAME}" "${HOST_NAME}" "${PASSWORD}"