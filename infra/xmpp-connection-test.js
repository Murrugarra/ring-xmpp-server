const { client, xml } = require("@xmpp/client");
const debug = require("@xmpp/debug");
const e = require("express");

process.env.NODE_TLS_REJECT_UNAUTHORIZED=0;

const xmppPort=49342

// service: "xmpp://165.232.90.255:5222/",
const xmpp = client({
    service: "xmpp://localhost:" + xmppPort,
    domain: "konoha",
    username: "kakashi",
    password: "p4ssword",
});

debug(xmpp, true);

xmpp.on("error", (err) => {
    console.error(err);
});

xmpp.on("offline", () => {
    console.log("offline");
});

xmpp.on("stanza", async (stanza) => {
    if (stanza.is("message")) {
        await xmpp.send(xml("presence", { type: "unavailable" }));
        // await xmpp.stop();
    }
});

xmpp.on("online", async (address) => {
    // Makes itself available
    console.log("CHEVEREEEE")
    await xmpp.send(xml("presence"));

    // Sends a chat message to itself
    const message = xml(
        "message",
        { type: "chat", to: address },
        xml("body", {}, "hello world"),
    );
    await xmpp.send(message);
});

xmpp.start().catch(console.error);
