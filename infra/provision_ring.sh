#!/bin/bash

## input
RING_DOMAIN_NAME=${1}
RING_ADMIN_USER=${2}
RING_ADMIN_PASSWORD=${3}

## files
WORKDIR="$(pwd)"
TEMP_DIR="${WORKDIR}/tmp"
TEMPLATE_DIR="${WORKDIR}/templates"
CURRENT_CONFIG_TEMPLATE_FILE="${TEMPLATE_DIR}/ejabberd_alpha.yml"
TEMP_CONFIG_FILE="${TEMP_DIR}/ejabberd.${RING_DOMAIN_NAME}.yml"
TEMP_CONFIG_FILE_READY="${TEMP_DIR}/ejabberd.yml"

## container
RING_SERVER_DOCKER_IMAGE="ring-xmpp-server:alpha"
RING_CONTAINER_INSPECT_FILE="${TEMP_DIR}/${RING_DOMAIN_NAME}-inspect.json"
RING_CONTAINER_NAME="ring-${RING_DOMAIN_NAME}"

create_temp_dir() {
  log "Setting temporary dir: ${TEMP_DIR}"
  mkdir -p "${TEMP_DIR}"
}

prepare_ejabberd_file() {
  log "Preparing config file at ${TEMP_CONFIG_FILE}"
  cp "${CURRENT_CONFIG_TEMPLATE_FILE}" "${TEMP_CONFIG_FILE}"
  sed  's/RING_DOMAIN_NAME/'"${RING_DOMAIN_NAME}"'/g ; s/RING_ADMIN_USER/'"${RING_ADMIN_USER}"'/g' "${TEMP_CONFIG_FILE}" > "${TEMP_CONFIG_FILE_READY}"
}

run_container() {
  ## TODO: Fix ports
  container_id=$(docker run -d\
                            -P\
                            --name "${RING_CONTAINER_NAME}"\
                            -v "${TEMP_CONFIG_FILE_READY}":/home/ejabberd/conf/ejabberd.yml\
                            "${RING_SERVER_DOCKER_IMAGE}")

  log "Created container with Id: ${container_id}"

  docker inspect "${container_id}" > "${RING_CONTAINER_INSPECT_FILE}"

  container_xmpp_port="$(jq -c '.[] | .NetworkSettings.Ports."5222/tcp" | .[] | .HostPort' "${RING_CONTAINER_INSPECT_FILE}" | sed 's/"//g' )"
  container_http_api_port="$(jq -c '.[] | .NetworkSettings.Ports."5281/tcp" | .[] | .HostPort' "${RING_CONTAINER_INSPECT_FILE}" | sed 's/"//g')"

  log "Container XMPP Port: ${container_xmpp_port}"
  log "Container API Port: ${container_http_api_port}"

}

register_admin_user_in_container() {
  log "Wait 5 seconds until container is started ..."
  sleep 5s

  log "Registering admin user for domain"
  log "$(docker exec -it "${RING_CONTAINER_NAME}" bin/ejabberdctl register "${RING_ADMIN_USER}" "${RING_DOMAIN_NAME}" "${RING_ADMIN_PASSWORD}")"
}

clean() {
  rm "${TEMP_CONFIG_FILE}" "${RING_CONTAINER_INSPECT_FILE}" "${TEMP_CONFIG_FILE_READY}"
  log "Removed temporary files"
}

log() {
  echo "$(date -u) INFO: ${1}"
}

warn() {
  echo "$(date -u) WARN: ${1}"
}

kill_containers() {
  warn "Removing all containers. THIS SHOULD NOT BE PRINTED IF YOU ARE IN PRODUCTION!"
  docker rm -f $(docker ps -aq) > /dev/null # TODO: Quitar esto
}

main() {
  log "Ring Provisioning started for domain [${RING_DOMAIN_NAME}]"
  kill_containers
  create_temp_dir
  prepare_ejabberd_file
  run_container
  register_admin_user_in_container
  clean
  log "Provisioning finished"
}

main


# TODO: PONER NGINX EN FRENTE
# docker inspect ring-kaze | jq -c '.[] | .NetworkSettings.Ports."5280/tcp" | .[] | .HostPort' | sed 's/"//g'

# TODO: Pasar configuration por variables de entorno
# TODO: Encapsular CLI