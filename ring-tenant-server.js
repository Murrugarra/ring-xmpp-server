const express = require("express")
const cors = require("cors")
const mongoose = require("mongoose")
const spawn = require("child_process").spawn
const app = express()

app.use(express.json())
app.use(cors())

// TODO: Fix issue with nodejs child_process don't recognize sleep command, change for while maybe

const RingSchema = new mongoose.Schema({
  name: String,
  domain: String,
  creator: String,
  admin: String,
  password: String,
  ports: {
    xmpp: {type: Number, default: 0},
    api: {type: Number, default: 0}
  },
  status: {
    type: String,
    enum: ["REQUESTED", "PROCESSING", "FAILED", "COMPLETED"],
    default: "REQUESTED"
  },
  tags: {
    type: [String]
  },
  containerId: { type: String },
  private: { type: Boolean, default: false },
  createdAt: {type: Date, default: Date.now }
})

const Ring = mongoose.model("ring", RingSchema)

app.route("/v1/rings")
  .get(async (req, res) => {
    const rings = await Ring.find()
    res.json(rings)
  })
  .post(async (req, res) => {
    let ring = new Ring(req.body)
    // TODO: Validate ringData

    ring = await ring.save()

    const child = spawn('./infra2/provision.sh', 
	  [ring.domain, ring.admin, ring.password, ring._id], {detached: true})

    res.json(ring._id)
})

app.route("/v1/rings/:id/status")
  .put(async (req, res) => {
    // TODO: Add validation if ring exists or not
    let ring = await Ring.findOne({ _id: req.params.id })
    const {status, xmppPort, apiPort, containerId} = req.query

    ring.ports.xmpp = xmppPort || 0
    ring.ports.api =  apiPort || 0
    ring.containerId = containerId || ''
    ring.status=status

    ring = await ring.save()
    
    res.send("Privioning status updated successfully.")
})


const main = async () => {
  await mongoose.connect("mongodb://localhost:27017/ring")
  console.log("Connected to MongoDB")

  app.listen(4000, () => {
    console.log(`App is running on port 4000`)
  })
}

main()
