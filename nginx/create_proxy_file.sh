#!/bin/bash
SERVER_PORT=10000
PROTOCOL=http
DOMAIN="${1}"
TARGET="${2}"
TEMPLATE_LOCATION=$(pwd)/nginx_template.conf
LOCAL_NGINX_CONF_D=/home/nobu/Projects/Ring/ring-xmpp-server/nginx/conf.d/

CONFIG_FILE="${LOCAL_NGINX_CONF_D}"/"${DOMAIN}.conf"

sed 's/DOMAIN/'"${DOMAIN}"'/g; s/TARGET/'"${TARGET}"'/g; s/PROTOCOL/'"${PROTOCOL}"'/g' $TEMPLATE_LOCATION > $CONFIG_FILE

#nginx -s reload