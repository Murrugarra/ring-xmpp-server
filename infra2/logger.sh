fail() {
  log "CRITICAL" "${1}"
  exit 1
}

info() {
  log "INFO" "${1}"
}

warn() {
  log "WARN" "${1}"
}

log() {
  message="[$(date -u)][${1}]: ${2}"
  echo "${message}" >> "${RING_LOG_FILE}"
  echo "${message}"
}
