#!/bin/bash

DATA=${1} # location to redirect
NAME=${2} # subdomain name

TARGET_DOMAIN="bytesincolor.com"
RECORDS_API_ENDPOINT="https://api.godaddy.com/v1/domains/${TARGET_DOMAIN}/records"
API_KEY="dKDLmeVGVQDr_JGeYgJtwfKDJWjxGhKz1HL:JGQiWH1hCanZCfqkRZAgsE"



curl -X PATCH -H "Content-Type: application/json" \
-H "Authorization: sso-key ${API_KEY}" \
"${RECORDS_API_ENDPOINT}" \
--data-binary @- <<EOF
[
    {
        "type": "A", 
        "ttl": 3600, 
        "data": "$DATA", 
        "name": "$NAME"
    }
]
EOF