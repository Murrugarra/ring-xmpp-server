#!/bin/bash

# Imports
. "/app/logger.sh"

# Constants
CONFIG_FILE="/app/ejabberd_alpha_v2.yml"

if [ -z "${RING_DOMAIN}" ]; then
  fail "Set variable [RING_DOMAIN]."
fi

# Use admin:admin if env variables where not passed
RING_ADMIN_USER="${RING_ADMIN_USER:-admin}"
RING_ADMIN_PASSWORD="${RING_ADMIN_PASSWORD:-admin}"

# Replace config
sed -i.bak 's/RING_DOMAIN/'"${RING_DOMAIN}"'/g ; s/ADMIN_USER/'"${RING_ADMIN_USER}"'/g' "${CONFIG_FILE}"

cp "${CONFIG_FILE}" /home/ejabberd/conf/ejabberd.yml

# TODO: Create User antes de que arranque? o despues, tal vez en una shell sola levantar y crear el usuario.


# https://hub.docker.com/layers/ecs/ejabberd/ecs/21.12/images/sha256-01d056c057070e66c2d4901753fdb23bf6fc09c1fe2b91ab2ad382ee2f77d296?context=explore
# Run ejabbed
/home/ejabberd/bin/ejabberdctl foreground