#!/bin/bash

# TODO: Validate ring is unique [maybe in backend]

RING_DOMAIN="${1}"
RING_ADMIN_USER="${2}"
RING_ADMIN_PASSWORD="${3}"
RING_ID="${4}"

RING_SERVER="${RING_SERVER:-localhost:4000}"
RING_IMAGE="ring-server:alpha-0.0.1"
RING_LOG_DIR="${HOME}/Projects/Ring/logs/rings"
RING_LOG_FILE="${RING_LOG_DIR}/${RING_DOMAIN}.log"

TARGET_DOMAIN="bytesincolor.com"
RECORDS_API_ENDPOINT="https://api.godaddy.com/v1/domains/${TARGET_DOMAIN}/records"
API_KEY="dKDLmeVGVQDr_JGeYgJtwfKDJWjxGhKz1HL:JGQiWH1hCanZCfqkRZAgsE" #TODO: replace with environment var from .profile file

. "./logger.sh"

if [ ! -d "${RING_LOG_DIR}" ]; then
  mkdir -p "${RING_LOG_DIR}"
fi

#if [ ! -f "${RING_LOG_FILE}" ]; then
#  touch ${RING_LOG_FILE}
#fi

echo -n "" > ${RING_LOG_FILE}

if [ ! -z "${RING_ID}" ]; then
  info "- Env:"
  info "- Ring Id: ${RING_ID}"
  info "- Ring Server: ${RING_SERVER}"

  info "✓ $(curl -s -X PUT "${RING_SERVER}/v1/rings/${RING_ID}/status?status=PROCESSING")"
fi

info "* Provisioning started for domain [${RING_DOMAIN}]..."


info "* Logs location: ${RING_LOG_FILE}"

container_id=$(docker run -d -P\
		      --name ${RING_DOMAIN}\
                      -e RING_DOMAIN="${RING_DOMAIN}"\
                      -e RING_ADMIN_USER="${RING_ADMIN_USER}"\
                      -e RING_ADMIN_PASSWORD="${RING_ADMIN_PASSWORD}"\
                      ${RING_IMAGE})

info "✓ Container created Id: ${container_id}."
info "* Registering admin user..."

sleep 5s

info "* $(docker exec "${container_id}" bin/ejabberdctl register "${RING_ADMIN_USER}" "${RING_DOMAIN}" "${RING_ADMIN_PASSWORD}")"
info "✓ Admin user registered."
info "* Resolving container ports."

inspect_file=$(mktemp)

docker inspect "${container_id}" > "${inspect_file}"

xmpp_port="$(jq -c '.[] | .NetworkSettings.Ports."5222/tcp" | .[] | .HostPort' "${inspect_file}" | head -1 | sed 's/"//g' )"
api_port="$(jq -c '.[] | .NetworkSettings.Ports."5281/tcp" | .[] | .HostPort' "${inspect_file}" | head -1 | sed 's/"//g')"

info "* Registering ring subdomain."

DATA="1.1.1.1" # TODO: Replace it for the server instance.

curl -X PATCH -H "Content-Type: application/json" \
-H "Authorization: sso-key ${API_KEY}" \
"${RECORDS_API_ENDPOINT}" \
--data-binary @- <<EOF
[
  {
    "type": "A", 
    "ttl": 3600, 
    "data": "$DATA", 
    "name": "$RING_DOMAIN"
  }
]
EOF

info "✓ Registering ring subdomain."

if [ ! -z "${RING_ID}" ]; then
  info "✓ $(curl -s -X PUT "${RING_SERVER}/v1/rings/${RING_ID}/status?status=COMPLETED&xmppPort=${xmpp_port}&apiPort=${api_port}&containerId=${container_id}")"
fi

info "✓ Ports [XMPP=${xmpp_port}] [API=${api_port}]"
