#!/bin/bash

mongo_container=$(docker ps | grep mongo | awk '{{ print $1}}')

docker exec -it $mongo_container bash
