#!/bin/bash


docker rm -f $(docker ps -a --format "{{.ID}}\t{{.Image}}\t{{.Names}}" | grep ring-server | awk '{{ print $1 }}')
